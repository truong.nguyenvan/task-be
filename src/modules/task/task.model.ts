import { Field, ID, ObjectType, registerEnumType } from '@nestjs/graphql';
import { TASK_STATUS } from '@prisma/client';

import { TaskGroup } from '@modules/taskGroup';
import { User } from '@modules/user';

// registerEnumType is a helper function that allows you to map your enum to the GraphQL schema.
registerEnumType(TASK_STATUS, {
  name: 'TASK_STATUS',
});

@ObjectType()
export class Task {
  // --------------------- Core --------------------- //
  @Field(() => ID)
  id: string;

  @Field(() => Date)
  createdAt: Date;

  @Field(() => Date)
  updatedAt: Date;

  @Field(() => String)
  createdBy: string;

  @Field(() => String)
  updatedBy: string;

  // --------------------- Properties --------------------- //
  @Field(() => String)
  name: string;

  @Field(() => String, { nullable: true })
  description: string;

  @Field(() => Number)
  estimateHours: number;

  @Field(() => TASK_STATUS)
  status: TASK_STATUS;

  @Field(() => [User])
  members: User[];

  // --------------------- Relations --------------------- //
  @Field(() => TaskGroup)
  taskGroup: TaskGroup;
}
