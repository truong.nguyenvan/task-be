import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { differenceInMilliseconds } from 'date-fns';
import { PubSub } from 'graphql-subscriptions';
import { filter, groupBy, isEmpty, isEqual, map } from 'lodash/fp';

import {
  CreateInputParams,
  DeleteInputParams,
  FindParams,
  UserParams,
} from '@common/types';
import { PrismaService } from '@core/prisma';

import { CreateTaskInput, FindTasksArgs, UpdateTaskInput } from './task.dto';
import { Task } from './task.model';

type BuildTaskType = 'NEW_TASK' | 'TASK_STATE_CHANGED' | 'TASKS_UN_COMPLETED';

type BuildTaskTopic = {
  userId: string;
  type: BuildTaskType;
};

@Injectable()
export class TaskService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly pubSub: PubSub,
  ) {}

  private async validateTaskInput(
    payload: CreateTaskInput | UpdateTaskInput,
  ): Promise<void> {
    const { estimateHours } = payload;

    // Validate estimate hours should be greater than 5 minutes
    if (estimateHours * 60 < 5)
      throw new Error('Estimate hours must be greater than 5 minutes');
  }

  public async finAll({ user }: UserParams): Promise<Task[]> {
    const targetTasks = await this.prisma.task.findMany({
      where: {
        userId: user.id,
      },
      orderBy: {
        createdAt: 'desc',
      },
      include: {
        taskGroup: true,
      },
    });

    return map((task) => {
      return {
        ...task,
        members: [user], // TO-DO: Implement members
      };
    }, targetTasks);
  }

  public async tasksByGroup({
    user,
    payload,
  }: FindParams<FindTasksArgs>): Promise<Task[]> {
    const targetTasks = await this.prisma.task.findMany({
      where: {
        userId: user.id,
        taskGroupId: payload.taskGroupId,
      },
      include: {
        taskGroup: true,
      },
    });

    return map((task) => {
      return {
        ...task,
        members: [user], // TO-DO: Implement members
      };
    }, targetTasks);
  }

  public buildTaskTopic({ userId, type }: BuildTaskTopic): string {
    return `${userId}-${type}`;
  }

  public async createOne({
    user,
    payload,
  }: CreateInputParams<CreateTaskInput>): Promise<Task> {
    await this.validateTaskInput(payload);

    const { name, description, estimateHours, taskGroupId } = payload;

    const createdTask = await this.prisma.task.create({
      data: {
        name,
        description,
        estimateHours,
        createdBy: user.id,
        updatedBy: user.id,
        user: { connect: { id: user.id } },
        taskGroup: taskGroupId && { connect: { id: taskGroupId } },
      },
      include: {
        taskGroup: true,
      },
    });

    const topic = this.buildTaskTopic({
      userId: user.id,
      type: 'NEW_TASK',
    });

    this.pubSub.publish(topic, {
      userId: user.id,
      onCreateTask: createdTask,
    });

    return {
      ...createdTask,
      members: [user], // TO-DO: Implement members
    };
  }

  public async updateOne({
    user,
    payload,
  }: CreateInputParams<UpdateTaskInput>): Promise<Task> {
    await this.validateTaskInput(payload);

    const { id, ...data } = payload;

    const targetTask = await this.prisma.task.findUnique({
      where: { userId: user.id, id },
    });

    if (!targetTask) throw new Error('Task not found');

    const updatedTask = await this.prisma.task.update({
      where: { userId: user.id, id },
      data: {
        ...data,
        updatedBy: user.id,
      },
      include: {
        taskGroup: true,
      },
    });

    if (!isEqual(targetTask.status, updatedTask.status)) {
      const topic = this.buildTaskTopic({
        userId: user.id,
        type: 'TASK_STATE_CHANGED',
      });

      this.pubSub.publish(topic, {
        userId: user.id,
        onUpdateStatusTask: updatedTask,
      });
    }

    return {
      ...updatedTask,
      members: [user], // TO-DO: Implement members
    };
  }

  // Run every 5 minutes
  @Cron(CronExpression.EVERY_5_MINUTES)
  public async unCompleteTasks(): Promise<void> {
    const targetTasks = await this.prisma.task.findMany({
      where: {
        status: 'PENDING',
      },
    });

    // Currently, only support for pending tasks
    const overduePendingTasks = filter(
      ({ updatedAt, estimateHours }) =>
        differenceInMilliseconds(new Date(), updatedAt) >
        estimateHours * 60 * 60 * 1000, // Convert estimateHours to milliseconds when tasks are PENDING
      targetTasks,
    );

    const overdueTasks = [...overduePendingTasks];

    if (!isEmpty(overdueTasks)) {
      // Update status of tasks that have been created for more than created time + estimate time to UN_COMPLETED
      await this.prisma.task.updateMany({
        where: {
          id: {
            in: map(({ id }) => id, overdueTasks),
          },
        },
        data: {
          status: 'UN_COMPLETED',
        },
      });

      const groupedByUserId = groupBy('userId', overdueTasks);

      for (const key in groupedByUserId) {
        const topic = this.buildTaskTopic({
          userId: key,
          type: 'TASKS_UN_COMPLETED',
        });

        const targetUnCompletedTasks = await this.prisma.task.findMany({
          where: {
            id: {
              in: map('id', groupedByUserId[key]),
            },
          },
        });

        this.pubSub.publish(topic, {
          userId: key,
          onUnCompleteTasks: targetUnCompletedTasks,
        });
      }
    }
  }

  public async deleteOne({ user, id }: DeleteInputParams): Promise<boolean> {
    const targetTask = await this.prisma.task.findUnique({
      where: { userId: user.id, id },
    });

    if (!targetTask) throw new Error('Task not found');

    await this.prisma.task.delete({
      where: { id },
    });

    return true;
  }
}
