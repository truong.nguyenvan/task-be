import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver, Subscription } from '@nestjs/graphql';
import { PubSub } from 'graphql-subscriptions';

import { CurrentUser } from '@common/decorators';
import { AuthGuard } from '@common/guard';
import { SubscriptionContext, SubscriptionPayload } from '@common/types';
import { User } from '@modules/user';

import { CreateTaskInput, FindTasksArgs, UpdateTaskInput } from './task.dto';
import { Task } from './task.model';
import { TaskService } from './task.service';

type UnCompleteTasksPayload = SubscriptionPayload & {
  onUnCompleteTasks: Task[];
};

type UpdateStatusTaskPayload = SubscriptionPayload & {
  onUpdateStatusTask: Task;
};

type CreateTaskPayload = SubscriptionPayload & {
  onCreateTask: Task;
};

@Resolver()
export class TaskResolver {
  constructor(
    private readonly service: TaskService,
    private readonly pubSub: PubSub,
  ) {}

  @UseGuards(AuthGuard)
  @Query(() => [Task])
  public tasks(@CurrentUser() user: User): Promise<Task[]> {
    return this.service.finAll({ user });
  }

  @UseGuards(AuthGuard)
  @Query(() => [Task])
  public async tasksByGroup(
    @CurrentUser() user: User,
    @Args('payload') { taskGroupId }: FindTasksArgs,
  ) {
    return this.service.tasksByGroup({ user, payload: { taskGroupId } });
  }

  @UseGuards(AuthGuard)
  @Mutation(() => Task)
  public createTask(
    @CurrentUser() user: User,
    @Args('payload') payload: CreateTaskInput,
  ): Promise<Task> {
    return this.service.createOne({ user, payload });
  }

  @UseGuards(AuthGuard)
  @Mutation(() => Task)
  public updateTask(
    @CurrentUser() user: User,
    @Args('payload') payload: UpdateTaskInput,
  ): Promise<Task> {
    return this.service.updateOne({ user, payload });
  }

  @UseGuards(AuthGuard)
  @Mutation(() => Boolean)
  public deleteTask(
    @CurrentUser() user: User,
    @Args('id') id: string,
  ): Promise<boolean> {
    return this.service.deleteOne({ user, id });
  }

  @Subscription(() => Task, {
    filter(
      { userId }: CreateTaskPayload,
      _,
      { req: { user } }: SubscriptionContext,
    ) {
      return userId === user.id || user.role === 'ADMIN';
    },
  })
  @UseGuards(AuthGuard)
  public onCreateTask(@CurrentUser() user: User) {
    const topic = this.service.buildTaskTopic({
      userId: user.id,
      type: 'NEW_TASK',
    });

    return this.pubSub.asyncIterator<Task>(topic);
  }

  @Subscription(() => Task, {
    filter(
      { userId }: UpdateStatusTaskPayload,
      _,
      { req: { user } }: SubscriptionContext,
    ) {
      return userId === user.id || user.role === 'ADMIN';
    },
  })
  @UseGuards(AuthGuard)
  public onUpdateStatusTask(@CurrentUser() user: User) {
    const topic = this.service.buildTaskTopic({
      userId: user.id,
      type: 'TASK_STATE_CHANGED',
    });

    return this.pubSub.asyncIterator<Task>(topic);
  }

  @Subscription(() => [Task], {
    filter(
      { userId }: UnCompleteTasksPayload,
      _,
      { req: { user } }: SubscriptionContext,
    ) {
      return userId === user.id;
    },
  })
  @UseGuards(AuthGuard)
  public onUnCompleteTasks(@CurrentUser() user: User) {
    const topic = this.service.buildTaskTopic({
      userId: user.id,
      type: 'TASKS_UN_COMPLETED',
    });

    return this.pubSub.asyncIterator<Task[]>(topic);
  }
}
