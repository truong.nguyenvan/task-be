import { Field, InputType } from '@nestjs/graphql';
import { TASK_STATUS } from '@prisma/client';
import { IsString } from 'class-validator';

@InputType()
export class FindTasksArgs {
  @Field(() => String)
  @IsString()
  taskGroupId: string;
}

@InputType()
export class CreateTaskInput {
  @Field(() => String)
  @IsString()
  name: string;

  @Field(() => String, { nullable: true })
  @IsString()
  description?: string;

  @Field(() => Number, { defaultValue: 0 })
  estimateHours: number;

  @Field(() => TASK_STATUS, { nullable: true })
  status: TASK_STATUS;

  @Field(() => String)
  taskGroupId: string;
}

@InputType()
export class UpdateTaskInput extends CreateTaskInput {
  @Field(() => String)
  @IsString()
  id: string;
}
