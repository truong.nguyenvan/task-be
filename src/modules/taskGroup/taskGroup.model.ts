import { Field, ID, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class TaskGroup {
  // --------------------- Core --------------------- //
  @Field(() => ID)
  id: string;

  @Field(() => Date)
  createdAt: Date;

  @Field(() => Date)
  updatedAt: Date;

  @Field(() => String)
  createdBy: string;

  @Field(() => String)
  updatedBy: string;

  // --------------------- Properties --------------------- //
  @Field(() => String)
  name: string;
}
