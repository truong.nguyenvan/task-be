import { Field, InputType } from '@nestjs/graphql';
import { IsString } from 'class-validator';

@InputType()
export class FindTaskGroupArgs {
  @Field(() => String)
  @IsString()
  id: string;
}

@InputType()
export class CreateTaskGroupInput {
  @Field(() => String)
  @IsString()
  name: string;
}

@InputType()
export class UpdateTaskGroupInput extends CreateTaskGroupInput {
  @Field(() => String)
  @IsString()
  id: string;
}
