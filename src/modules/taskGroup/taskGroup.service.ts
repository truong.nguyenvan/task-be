import { Injectable } from '@nestjs/common';

import {
  CreateInputParams,
  DeleteInputParams,
  FindParams,
  UpdateInputParams,
  UserParams,
} from '@common/types';
import { PrismaService } from '@core/prisma';

import {
  CreateTaskGroupInput,
  FindTaskGroupArgs,
  UpdateTaskGroupInput,
} from './taskGroup.dto';
import { TaskGroup } from './taskGroup.model';

@Injectable()
export class TaskGroupService {
  constructor(private readonly prisma: PrismaService) {}

  public async findAll({ user }: UserParams): Promise<TaskGroup[]> {
    const targetTaskGroups = await this.prisma.taskGroup.findMany({
      where: { userId: user.id },
    });

    return targetTaskGroups;
  }

  public async findOne({
    user,
    payload,
  }: FindParams<FindTaskGroupArgs>): Promise<TaskGroup> {
    const targetTaskGroup = await this.prisma.taskGroup.findUnique({
      where: { userId: user.id, id: payload.id },
    });

    if (!targetTaskGroup) throw new Error('TaskGroup not found');

    return targetTaskGroup;
  }

  public async createOne({
    user,
    payload,
  }: CreateInputParams<CreateTaskGroupInput>): Promise<TaskGroup> {
    const createdTaskGroup = await this.prisma.taskGroup.create({
      data: {
        ...payload,
        createdBy: user.id,
        updatedBy: user.id,
        user: { connect: { id: user.id } },
      },
    });

    return createdTaskGroup;
  }

  public async updateOne({
    user,
    payload,
  }: UpdateInputParams<UpdateTaskGroupInput>): Promise<TaskGroup> {
    const { id, ...data } = payload;

    const targetTaskGroup = await this.prisma.taskGroup.findUnique({
      where: { userId: user.id, id },
    });

    if (!targetTaskGroup) throw new Error('TaskGroup not found');

    const updatedTaskGroup = await this.prisma.taskGroup.update({
      where: { userId: user.id, id: targetTaskGroup.id },
      data: { ...data, updatedBy: user.id },
    });

    return updatedTaskGroup;
  }

  public async deleteOne({ user, id }: DeleteInputParams): Promise<boolean> {
    const targetTaskGroup = await this.prisma.taskGroup.findUnique({
      where: { userId: user.id, id },
    });

    if (!targetTaskGroup) throw new Error('TaskGroup not found');

    await this.prisma.task.deleteMany({
      where: { taskGroupId: targetTaskGroup.id },
    });

    await this.prisma.taskGroup.delete({ where: { id } });

    return true;
  }
}
