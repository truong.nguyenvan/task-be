import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { CurrentUser } from '@common/decorators';
import { AuthGuard } from '@common/guard';
import { User } from '@modules/user';

import {
  CreateTaskGroupInput,
  FindTaskGroupArgs,
  UpdateTaskGroupInput,
} from './taskGroup.dto';
import { TaskGroup } from './taskGroup.model';
import { TaskGroupService } from './taskGroup.service';

@Resolver()
export class TaskGroupResolver {
  constructor(private readonly service: TaskGroupService) {}

  @UseGuards(AuthGuard)
  @Query(() => [TaskGroup])
  public taskGroups(@CurrentUser() user: User): Promise<TaskGroup[]> {
    return this.service.findAll({ user });
  }

  @UseGuards(AuthGuard)
  @Query(() => TaskGroup)
  public taskGroup(
    @CurrentUser() user: User,
    @Args('payload') payload: FindTaskGroupArgs,
  ): Promise<TaskGroup> {
    return this.service.findOne({ user, payload });
  }

  @UseGuards(AuthGuard)
  @Mutation(() => TaskGroup)
  public createTaskGroup(
    @CurrentUser() user: User,
    @Args('payload') payload: CreateTaskGroupInput,
  ): Promise<TaskGroup> {
    return this.service.createOne({ user, payload });
  }

  @UseGuards(AuthGuard)
  @Mutation(() => TaskGroup)
  public updateTaskGroup(
    @CurrentUser() user: User,
    @Args('payload') payload: UpdateTaskGroupInput,
  ): Promise<TaskGroup> {
    return this.service.updateOne({ user, payload });
  }

  @UseGuards(AuthGuard)
  @Mutation(() => Boolean)
  public deleteTaskGroup(@CurrentUser() user: User, @Args('id') id: string) {
    return this.service.deleteOne({ user, id });
  }
}
