import { Module } from '@nestjs/common';

import { TaskGroupResolver } from './taskGroup.resolver';
import { TaskGroupService } from './taskGroup.service';

@Module({
  exports: [],
  providers: [TaskGroupResolver, TaskGroupService],
})
export class TaskGroupModule {}
