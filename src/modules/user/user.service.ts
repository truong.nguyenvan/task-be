import { Injectable } from '@nestjs/common';

import { CreateInputParams, UpdateInputParams } from '@common/types';
import { hashPassword } from '@common/utils';
import { PrismaService } from '@core/prisma';

import { CreateUserInput, UpdateUserInput } from './user.dto';
import { User } from './user.model';

@Injectable()
export class UserService {
  constructor(private readonly prisma: PrismaService) {}

  public async findAll(): Promise<User[]> {
    return await this.prisma.user.findMany();
  }

  public async findOne(id: string): Promise<User> {
    const target = await this.prisma.user.findUnique({
      where: { id },
    });

    if (!target) throw new Error('User not found');

    return target;
  }

  public async createOne({
    user,
    payload,
  }: CreateInputParams<CreateUserInput>): Promise<User> {
    const target = await this.prisma.user.findFirst({
      where: { email: payload.email },
    });

    if (target) throw new Error('Email already exists');

    const password = await hashPassword(payload.password);

    return await this.prisma.user.create({
      data: {
        ...payload,
        createdBy: user.id,
        updatedBy: user.id,
        password,
      },
    });
  }

  public async updateOne({
    user,
    payload,
  }: UpdateInputParams<UpdateUserInput>): Promise<User> {
    const { id, ...data } = payload;

    const target = await this.prisma.user.findUnique({
      where: { id },
    });

    if (!target) throw new Error('User not found');

    return await this.prisma.user.update({
      where: { id },
      data: {
        ...data,
        updatedBy: user.id,
        password: await hashPassword(payload.password),
      },
    });
  }

  public async deleteOne(id: string): Promise<boolean> {
    const target = await this.prisma.user.findUnique({
      where: { id },
    });

    if (!target) throw new Error('User not found');

    await this.prisma.user.delete({
      where: { id },
    });

    return true;
  }
}
