import { Field, ID, ObjectType, registerEnumType } from '@nestjs/graphql';
import { ROLE } from '@prisma/client';

registerEnumType(ROLE, {
  name: 'ROLE',
});

@ObjectType()
export class User {
  // --------------------- Core --------------------- //
  @Field(() => ID)
  id: string;

  @Field(() => Date)
  createdAt: Date;

  @Field(() => Date)
  updatedAt: Date;

  // --------------------- Properties --------------------- //
  @Field(() => String)
  firstName: string;

  @Field(() => String)
  lastName: string;

  @Field(() => String, { nullable: true })
  avatar: string;

  @Field(() => String)
  email: string;

  @Field(() => ROLE)
  role: ROLE;
}
