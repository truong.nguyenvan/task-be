import { Field, InputType, OmitType } from '@nestjs/graphql';
import { ROLE } from '@prisma/client';
import { IsEmail, IsEnum, IsString } from 'class-validator';

@InputType()
export class CreateUserInput {
  @Field()
  @IsString()
  firstName: string;

  @Field()
  @IsString()
  lastName: string;

  @Field()
  @IsEmail()
  email: string;

  @Field()
  @IsString()
  password: string;

  @Field(() => ROLE, { nullable: true })
  @IsEnum([ROLE.ADMIN, ROLE.USER])
  role: ROLE;
}

@InputType()
export class UpdateUserInput extends OmitType(CreateUserInput, ['email']) {
  @Field()
  @IsString()
  id: string;
}
