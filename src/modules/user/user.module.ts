import { Module } from '@nestjs/common';

import { AuthGuard } from '@common/guard';

import { UserResolver } from './user.resolver';
import { UserService } from './user.service';

@Module({
  providers: [UserResolver, UserService, AuthGuard],
})
export class UserModule {}
