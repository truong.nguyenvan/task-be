import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { CurrentUser } from '@common/decorators';
import { AdminGuard, AuthGuard } from '@common/guard';

import { CreateUserInput, UpdateUserInput } from './user.dto';
import { User } from './user.model';
import { UserService } from './user.service';

@Resolver()
export class UserResolver {
  constructor(private readonly service: UserService) {}

  @UseGuards(AdminGuard)
  @Query(() => [User])
  public users(): Promise<User[]> {
    return this.service.findAll();
  }

  @UseGuards(AdminGuard)
  @Query(() => User)
  public user(@Args('id') id: string): Promise<User> {
    return this.service.findOne(id);
  }

  @UseGuards(AdminGuard)
  @Mutation(() => User)
  public createUser(
    @CurrentUser() user: User,
    @Args('payload') createUserInput: CreateUserInput,
  ): Promise<User> {
    return this.service.createOne({ user, payload: createUserInput });
  }

  @UseGuards(AdminGuard)
  @Mutation(() => User)
  public updateUser(
    @CurrentUser() user: User,
    @Args('payload') updateUserInput: UpdateUserInput,
  ) {
    return this.service.updateOne({
      user,
      payload: updateUserInput,
    });
  }

  @UseGuards(AdminGuard)
  @Mutation(() => Boolean)
  public deleteUser(@Args('id') id: string): Promise<boolean> {
    return this.service.deleteOne(id);
  }

  @UseGuards(AuthGuard)
  @Query(() => User)
  public me(@CurrentUser() user: User): Promise<User> {
    return this.service.findOne(user?.id);
  }
}
