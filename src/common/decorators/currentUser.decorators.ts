import { createParamDecorator } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';

import { User } from '@modules/user';

export const CurrentUser = createParamDecorator(
  (_, ctx): User => GqlExecutionContext.create(ctx).getContext().req.user,
);
