import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { JwtService } from '@nestjs/jwt';
import { Request } from 'express';

import { AppConfig } from '@core/config';
import { User } from '@modules/user';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private readonly jwtService: JwtService,
    private readonly appConfig: AppConfig,
  ) {}

  private extractTokenFromHeader(request: Request): string | undefined {
    const [type, token] = request?.headers.authorization?.split(' ') ?? [];
    return type === 'Bearer' ? token : undefined;
  }

  public async getUserFromContext(ctx: ExecutionContext): Promise<User> {
    const request = GqlExecutionContext.create(ctx).getContext().req;
    const token = this.extractTokenFromHeader(request);

    if (!token) throw new Error('Token not found');

    const target = await this.jwtService.verifyAsync<User>(token, {
      secret: this.appConfig.jwtAccessSecret,
    });

    if (!target) throw new UnauthorizedException();

    // 💡 We're assigning the payload to the request object here
    // so that we can access it in our route handlers
    request['user'] = target;

    return target;
  }

  public async canActivate(ctx: ExecutionContext): Promise<boolean> {
    await this.getUserFromContext(ctx);
    return true;
  }
}
