import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';

import { AuthGuard } from './auth.guard';

@Injectable()
export class AdminGuard implements CanActivate {
  constructor(private readonly authGuard: AuthGuard) {}

  public async canActivate(ctx: ExecutionContext): Promise<boolean> {
    const target = await this.authGuard.getUserFromContext(ctx);
    if (target.role === 'ADMIN') return true;
  }
}
