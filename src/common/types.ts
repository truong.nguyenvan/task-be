import { User } from '@modules/user';

// ---------------- Graphql ----------------//
export interface SubscriptionParams extends Record<string, unknown> {
  authorization: Record<string, string>;
}

export interface SubscriptionPayload {
  userId: string;
}

export interface SubscriptionContext {
  req: {
    user: User;
  };
}

// ---------------- CRUD Params ----------------//
export interface UserParams {
  user?: User;
}

export interface PayloadParams<T> extends UserParams {
  payload: T;
}

export type FindParams<T> = PayloadParams<T>;

export type CreateInputParams<T> = PayloadParams<T>;

export type UpdateInputParams<T> = PayloadParams<T>;

export interface DeleteInputParams extends UserParams {
  id: string;
}
