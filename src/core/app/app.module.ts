import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { ScheduleModule } from '@nestjs/schedule';

import { AuthModule } from '@core/auth';
import { ConfigModule } from '@core/config';
import { GraphQLModule } from '@core/graphql';
import { PrismaModule } from '@core/prisma';
import { PubSubModule } from '@core/pubSub';
import { TaskModule } from '@modules/task';
import { TaskGroupModule } from '@modules/taskGroup';
import { UserModule } from '@modules/user';

@Module({
  imports: [
    GraphQLModule,
    JwtModule.register({
      global: true,
    }),
    ConfigModule,
    ScheduleModule.forRoot(),
    PrismaModule,
    AuthModule,
    PubSubModule,
    UserModule,
    TaskGroupModule,
    TaskModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
