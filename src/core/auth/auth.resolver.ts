import { Args, Mutation, Resolver } from '@nestjs/graphql';

import { LoginInput } from './auth.dto';
import { Auth } from './auth.model';
import { AuthService } from './auth.service';

@Resolver()
export class AuthResolver {
  constructor(private readonly service: AuthService) {}

  @Mutation(() => Auth)
  public login(
    @Args('payload') { email, password }: LoginInput,
  ): Promise<Auth> {
    return this.service.login({ payload: { email, password } });
  }
}
