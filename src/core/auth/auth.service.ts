import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { PayloadParams } from '@common/types';
import { comparePassword } from '@common/utils';
import { AppConfig } from '@core/config';
import { PrismaService } from '@core/prisma';

import { LoginInput } from './auth.dto';
import { Auth } from './auth.model';

@Injectable()
export class AuthService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly jwtService: JwtService,
    private readonly appConfig: AppConfig,
  ) {}

  public async login({
    payload: { email, password },
  }: PayloadParams<LoginInput>): Promise<Auth> {
    const target = await this.prisma.user.findFirst({
      where: { email },
    });

    if (!target) throw new Error('User not found');

    const isPasswordValid = await comparePassword(password, target.password);

    if (!isPasswordValid) throw new Error('Password is not valid');

    const [accessToken, refreshToken] = await Promise.all([
      this.jwtService.signAsync(
        { ...target },
        {
          secret: this.appConfig.jwtAccessSecret,
          expiresIn: '1d',
        },
      ),
      this.jwtService.signAsync(
        { ...target },
        { secret: this.appConfig.jwtRefreshSecret, expiresIn: '7d' },
      ),
    ]);

    return {
      accessToken,
      refreshToken,
    };
  }
}
