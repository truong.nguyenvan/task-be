import { Global, Module } from '@nestjs/common';
import { PubSub } from 'graphql-subscriptions';

@Global()
@Module({
  imports: [],
  providers: [
    {
      provide: PubSub,
      useValue: new PubSub(),
    },
  ],
  exports: [PubSub],
})
export class PubSubModule {}
