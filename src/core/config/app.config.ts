import { ApolloDriverConfig } from '@nestjs/apollo';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { SubscriptionParams } from '@common/types';

export const app = () =>
  <const>{
    jwtAccessSecret: process.env.JWT_ACCESS_SECRET,
    jwtRefreshSecret: process.env.JWT_REFRESH_SECRET,
  };

@Injectable()
export class AppConfig {
  private readonly appConfig: ReturnType<typeof app>;

  constructor(private readonly config: ConfigService) {
    this.appConfig = this.config.get('app');
  }

  public get jwtAccessSecret(): string {
    return this.appConfig.jwtAccessSecret;
  }

  public get jwtRefreshSecret(): string {
    return this.appConfig.jwtRefreshSecret;
  }

  public get gqlModuleOptions(): ApolloDriverConfig {
    return {
      autoSchemaFile: true,
      subscriptions: {
        'subscriptions-transport-ws': {
          onConnect: (connectionParams: SubscriptionParams) => {
            return {
              req: {
                headers: {
                  ...connectionParams,
                },
              },
            };
          },
        },
      }, // this work with graphql playground, just for development purpose
    };
  }
}
